package main

import "encoding/xml"

type StatusResponse struct {
	XMLName    xml.Name  `xml:"response"`
	Returncode string    `xml:"returncode"`
	MessageKey string    `xml:"messageKey"`
	Meetings   []Meeting `xml:"meetings>meeting"`
}

type Meeting struct {
	MeetingName           string `xml:"meetingName"`
	MeetingID             string `xml:"meetingID"`
	InternalMeetingID     string `xml:"internalMeetingID"`
	CreateTime            string `xml:"createTime"`
	CreateDate            string `xml:"createDate"`
	VoiceBridge           string `xml:"voiceBridge"`
	DialNumber            string `xml:"dialNumber"`
	AttendeePW            string `xml:"attendeePW"`
	ModeratorPW           string `xml:"moderatorPW"`
	Running               string `xml:"running"`
	Duration              string `xml:"duration"`
	HasUserJoined         string `xml:"hasUserJoined"`
	Recording             string `xml:"recording"`
	HasBeenForciblyEnded  string `xml:"hasBeenForciblyEnded"`
	StartTime             string `xml:"startTime"`
	EndTime               string `xml:"endTime"`
	ParticipantCount      string `xml:"participantCount"`
	ListenerCount         string `xml:"listenerCount"`
	VoiceParticipantCount string `xml:"voiceParticipantCount"`
	VideoCount            string `xml:"videoCount"`
	MaxUsers              string `xml:"maxUsers"`
	ModeratorCount        string `xml:"moderatorCount"`
	Attendees             string `xml:"attendees"`
	Metadata              string `xml:"metadata"`
	IsBreakout            string `xml:"isBreakout"`
}
