package main

import (
	"bytes"
	"crypto/sha1"
	"encoding/hex"
	"encoding/xml"
	"errors"
	"github.com/denisbrodbeck/machineid"
	"github.com/go-resty/resty/v2"
	"log"
	"net"
	"net/url"
	"os"
	"regexp"
	"strings"
	"time"
)

const (
	UrlRegex    = `URL:[\s]*(.+)`
	SecretRegex = `Secret:[\s]*(.+)`
)

var client = resty.New()
var (
	ClusterUrl  string
	urlRegex    *regexp.Regexp
	secretRegex *regexp.Regexp
	bbbConfig   *BbbConfig
)

type BbbConfig struct {
	ID     string
	Url    string
	Secret string
	IP     string
	Load   float64
}

func main() {
	log.Println("Starting Equilibrium Agent...")

	id, err := machineid.ID()
	if err != nil {
		log.Fatalf("unable to get machine id: %v", err)
	}

	log.Printf("Machine ID is '%v'", id)

	bbbConfig = &BbbConfig{
		ID: id,
	}

	ClusterUrl = GetEnv("EQUILIBRIUM_CLUSTER", "http://localhost:8080/equilibrium/agent")
	urlRegex = regexp.MustCompile(UrlRegex)
	secretRegex = regexp.MustCompile(SecretRegex)

	for {
		CallHome()

		time.Sleep(30 * time.Second)
	}
}

func GetEnv(key string, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}

	return fallback
}

func CallHome() {
	if err := ParseConfig(); err != nil {
		log.Println(err)
		return
	}

	GetStatus()

	log.Printf("Updating cluster at %v", ClusterUrl)

	resp, err := client.R().
		SetBody(bbbConfig).
		SetAuthToken("fookyou").
		Post(ClusterUrl)

	// if network error or non successful status code
	// error state
	if err != nil {
		log.Println(err)
		return
	}

	if resp.StatusCode() < 200 || resp.StatusCode() >= 300 {
		log.Println(resp.Status())
		return
	}

	log.Println(resp)
}

func ParseConfig() error {
	//cmd := exec.Command("bbb-conf", "--secret")
	//var stdout bytes.Buffer
	//cmd.Stdout = &stdout
	//
	//if err := cmd.Run(); err != nil {
	//	log.Fatal(err)
	//}

	stdout := bytes.NewBuffer([]byte{10, 32, 32, 32, 32, 85, 82, 76, 58, 32, 104, 116, 116, 112, 115, 58, 47, 47, 98, 98, 98, 46, 115, 112, 114, 105, 110, 103, 115, 99, 115, 46, 111, 114, 103, 47, 98, 105, 103, 98, 108, 117, 101, 98, 117, 116, 116, 111, 110, 47, 10, 32, 32, 32, 32, 83, 101, 99, 114, 101, 116, 58, 32, 108, 102, 79, 102, 104, 69, 107, 71, 78, 54, 77, 111, 65, 77, 69, 77, 65, 72, 57, 72, 56, 56, 107, 97, 51, 74, 68, 49, 119, 49, 86, 119, 120, 98, 88, 89, 69, 71, 99, 52, 105, 77, 10, 10, 32, 32, 32, 32, 76, 105, 110, 107, 32, 116, 111, 32, 116, 104, 101, 32, 65, 80, 73, 45, 77, 97, 116, 101, 58, 10, 32, 32, 32, 32, 104, 116, 116, 112, 115, 58, 47, 47, 109, 99, 111, 110, 102, 46, 103, 105, 116, 104, 117, 98, 46, 105, 111, 47, 97, 112, 105, 45, 109, 97, 116, 101, 47, 35, 115, 101, 114, 118, 101, 114, 61, 104, 116, 116, 112, 115, 58, 47, 47, 98, 98, 98, 46, 115, 112, 114, 105, 110, 103, 115, 99, 115, 46, 111, 114, 103, 47, 98, 105, 103, 98, 108, 117, 101, 98, 117, 116, 116, 111, 110, 47, 38, 115, 104, 97, 114, 101, 100, 83, 101, 99, 114, 101, 116, 61, 108, 102, 79, 102, 104, 69, 107, 71, 78, 54, 77, 111, 65, 77, 69, 77, 65, 72, 57, 72, 56, 56, 107, 97, 51, 74, 68, 49, 119, 49, 86, 119, 120, 98, 88, 89, 69, 71, 99, 52, 105, 77, 10, 10})

	urlMatches := urlRegex.FindStringSubmatch(stdout.String())
	secretMatches := secretRegex.FindStringSubmatch(stdout.String())

	if len(urlMatches) != 2 || len(secretMatches) != 2 {
		log.Printf("bbb-conf returned: %v", stdout)
		return errors.New("unable to parse secret or url, wrong length")
	}

	bbbConfig.Url = urlMatches[1]
	bbbConfig.Secret = secretMatches[1]
	bbbConfig.IP = GetOutboundIP()

	return nil
}

func GetOutboundIP() string {
	conn, err := net.Dial("udp", "8.8.8.8:80")
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	localAddr := conn.LocalAddr().(*net.UDPAddr)

	return localAddr.IP.String()
}

func GetStatus() *resty.Response {
	action := "getMeetings"

	b := sha1.Sum([]byte(action + bbbConfig.Secret))
	hash := hex.EncodeToString(b[:])

	u, _ := url.Parse(bbbConfig.Url)
	u.Path = strings.TrimRight(u.Path, "/") + "/api/" + action
	u.RawQuery = "checksum=" + hash

	uri := u.String()

	log.Printf("Polling server status at URL %v", uri)

	resp, err := client.R().Get(uri)
	if err != nil {
		log.Println(err)
	}

	var response StatusResponse
	err = xml.Unmarshal(resp.Body(), &response)
	if err != nil {
		log.Println(err)
	}

	bbbConfig.Load = float64(len(response.Meetings))

	return resp
}
