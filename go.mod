module go-equilibrium-agent

go 1.14

require (
	github.com/denisbrodbeck/machineid v1.0.1
	github.com/go-resty/resty/v2 v2.3.0
)
