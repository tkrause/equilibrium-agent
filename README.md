# Equilibrium Agent

The agent communicates back to the Equilibrium Cluster. Monitors the local node and communicates our status to the upstream server.

## Setup

- Build and deploy the agent on a BBB node. Can either be in Kubernetes or VM
- Create configuration file to set `EQUILIBRIUM_CLUSTER` to point to the Equilibrium Cluster Endpoint